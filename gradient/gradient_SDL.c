//gcc gradient_SDL.c -o gradient_SDL -lSDL

#include <stdio.h>
#include <SDL/SDL.h>

#define WIDTH 320
#define HEIGHT 240

#define MIN(a,b) (((a)<(b))?(a):(b))

void unsignedInt(SDL_Surface *bitmap)
{
    //Set the pixel
    unsigned int x=0, y=0;
    
    unsigned int *pixels = (unsigned int*)bitmap->pixels;
    unsigned int r=0, g=0, b=0, N=0, color=0;

    for(y=0; y<HEIGHT; y++)
    {
        for(x=0; x<WIDTH; x++)
        {
            /**
             * Because ints are stored from right to left, we need to 
             * bitshift each value the according amount of bits backwards.
             **/
            if (SDL_BYTEORDER == SDL_BIG_ENDIAN)
            {
                color = ((MIN(x, 255) & 0xff) << 16) + ((MIN(y, 255) & 0xff) << 8) + (b & 0xff);    //RGB
            }
            else
            {
                color = (b & 0xff)+((MIN(y, 255) & 0xff) << 8)+((MIN(x, 255) & 0xff) << 16);   //BGR
            }

            N = ( y * WIDTH)  + x;
            
            /**
             * Example: Yellow is 255,239,0 is "0xffef00"
             *
             * R (255 & 0xff) << 16 = ff0000
             * G (239 & 0xff) << 8 = ef00
             * B (0 & 0xff) = 0
             **/
            
            pixels[N] = color;
        }
    }
}

void unsignedChar(SDL_Surface *bitmap)
{
    //Set the pixel
    int x=0, y=0;
    unsigned int bpp = bitmap->format->BytesPerPixel;
    
    unsigned char* pixels = (unsigned char*)bitmap->pixels;
    unsigned char r=0, g=0, b=0, color=0;
    unsigned int N=0;
    
    //~ FILE *fp;
    //~ fp = fopen("out.bin","wb");

    for(y=0; y<HEIGHT; y++)
    {
        for(x=0; x<WIDTH; x++)
        {
            r = ((MIN(x, 255) & 0xff));
            g = ((MIN(y, 255) & 0xff));
            b = (b & 0xff);

            /**
             * RGB images are usually stored in interleaved order 
             * (R1, G1, B1, R2, G2, B2, ...)
             * 
             * Pixel with index N is stored at:
             *  pixels[bpp*N+1],
             *  pixels[bpp*N+2] 
             *  pixels[bpp*N+3] 
             * 
             * instead of just red[N], green[N], blue[N].
             * 
             * Example: Red is 255,0,0 is "0xff0000" 
             * 
             * A       R        G       B
             * 00      ff       00      00
             * bpp     bpp+1    bpp+2   bpp+3
             * 
             **/
            N = ( y * bitmap->w  + x );
            
            pixels[bpp * N + 0] = b;  //0 = BLUE
            pixels[bpp * N + 1] = g;  //1 = GREEN
            pixels[bpp * N + 2] = r;  //2 = RED
            pixels[bpp * N + 3] = 1;  //3 = ALPHA

            /**
             * Depending on the endianess, byte orders can be stored 
             * inverted in memory, and by fwrite too.
             *  
             * On 32bit little-endian systems we should have:
             * 00 00 ff 00
             **/

            //~ fwrite(&pixels[N * bpp], sizeof(unsigned char), 1, fp);
            //~ fwrite(&pixels[N * bpp+1], sizeof(unsigned char), 1, fp);
            //~ fwrite(&pixels[N * bpp+2], sizeof(unsigned char), 1, fp);
            //~ fwrite(&pixels[N * bpp+3], sizeof(unsigned char), 1, fp);
        }
    }
    
    //~ fclose(fp);
}

int main(int argc, char **argv)
{
    SDL_Init(SDL_INIT_VIDEO);
    SDL_Surface *screen;
    SDL_Surface *bitmap;

    SDL_Init(SDL_INIT_VIDEO);

    screen = SDL_SetVideoMode(WIDTH, HEIGHT, 16, SDL_SWSURFACE);

    bitmap = SDL_CreateRGBSurface(0,WIDTH, HEIGHT,32,0,0,0,0);
    
    //~ unsignedInt(bitmap);
    unsignedChar(bitmap);

    SDL_BlitSurface(bitmap, NULL, screen, NULL);
    SDL_Flip(screen);

    SDL_Event event;
    int quit=0;
    
    while(!quit)
    {
        SDL_PollEvent(&event);
        if (event.type == SDL_QUIT || (event.type == SDL_KEYDOWN && event.key.keysym.sym == SDLK_ESCAPE))
        {
            quit = 1;
        }
    }

    SDL_FreeSurface(bitmap);
    
    SDL_Quit();

    return 0;
}
