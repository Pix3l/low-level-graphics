// gcc gradient_X11.c -o gradient_X11 -lX11

#include <stdio.h>
#include <stdlib.h>
#include <X11/Xlib.h>
//~ #include <X11/Xutil.h>

#define WIDTH 320
#define HEIGHT 240

#define MIN(a,b) (((a)<(b))?(a):(b))

void unsignedInt(XImage *bitmap)
{
    unsigned int x=0, y=0;
    
    //Allocate our surface
    bitmap->data = malloc(WIDTH * HEIGHT * sizeof(unsigned int));
    unsigned int *pixels = (unsigned int*)bitmap->data;
    
    unsigned int r=0, g=0, b=0, N=0, color=0;

    for(y=0; y<HEIGHT; y++)
    {
        for(x=0; x<WIDTH; x++)
        {
            /**
             * Because ints are stored from right to left, we need to 
             * bitshift each value the according amount of bits backwards.
             **/
            if (bitmap->bitmap_bit_order == LSBFirst)
            {
				color = ((MIN(x, 255) & 0xff) << 16) + ((MIN(y, 255) & 0xff) << 8) + (b & 0xff);    //RGB
            }
			else
            {
                color = (b & 0xff)+((MIN(y, 255) & 0xff) << 8)+((MIN(x, 255) & 0xff) << 16);   //BGR
            }

            N = ( y * WIDTH)  + x;
            
            pixels[N] = color;
        }
    }
}

void unsignedChar(XImage *bitmap)
{
    unsigned int x=0, y=0;

    /**
     *  An RGBA pixel mean four bytes
     **/
    unsigned int bpp = 4;

    //Allocate our surface
    bitmap->data = (unsigned char *)malloc(WIDTH * HEIGHT * bpp);
    char* pixels = (char*)bitmap->data;
    
    unsigned char r=0, g=0, b=0, color=0;
    unsigned int N=0;

    for(y=0; y<HEIGHT; y++)
    {
        for(x=0; x<WIDTH; x++)
        {
            r = ((MIN(x, 255) & 0xff));
            g = ((MIN(y, 255) & 0xff));
            b = (b & 0xff);

            N = ( y * WIDTH) + x;

            /**
             * 32:
                Byte0: BBBBBBBB
                Byte1: GGGGGGGG
                Byte2: RRRRRRRR
                Byte3: 00000000

                16:
                Byte0: GGGBBBBB
                Byte1: RRRRRGGG (Note: LSB, bytes swapped)

                8:
                Byte0: BBGGGRRR
             **/

            pixels[bpp * N + 0] = b;  //1 = BLUE
            pixels[bpp * N + 1] = g;  //2 = GREEN
            pixels[bpp * N + 2] = r;  //3 = RED
            pixels[bpp * N + 3] = 1;  //0 = ALPHA
        }
    }
}

int main() 
{
    Display *dpy = XOpenDisplay(NULL);
    int scr = DefaultScreen(dpy);
    Window wnd = XCreateSimpleWindow(dpy, RootWindow(dpy, scr), 0, 0, WIDTH, HEIGHT, 0, BlackPixel(dpy, scr), WhitePixel(dpy, scr));
    XStoreName(dpy, wnd, "Gradient X11");
    XMapWindow(dpy, wnd);
    XSync(dpy, False);

    GC gc = XCreateGC(dpy, wnd, 0, 0);
    XImage *bitmap = XCreateImage(dpy, DefaultVisual(dpy, 0), 24, ZPixmap, 0, NULL, WIDTH, HEIGHT, 32, 0);

    //~ unsignedInt(bitmap);
    unsignedChar(bitmap);

    XEvent e;
    XSelectInput(dpy, wnd, KeyPressMask | KeyReleaseMask);

    int quit = 0;
    while(!quit) 
    {
        XNextEvent(dpy, &e);

        switch (e.type) 
        {
            case KeyPress:
            case KeyRelease:
                if( e.xkey.keycode == 0x09 )
                {
                    quit = 1;
                }
            default: 
                break;
        }

        XPutImage(dpy, wnd, gc, bitmap, 0, 0, 0, 0, WIDTH, HEIGHT);
        XFlush(dpy);
    }

    //~ XDestroyImage(bitmap);
    bitmap->f.destroy_image(bitmap);
    
    return XCloseDisplay(dpy);
}
