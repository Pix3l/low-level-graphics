//gcc gradient_alleg4.c -o gradient_alleg4 -lalleg

#include <stdio.h>
#include <allegro.h>

#define WIDTH 320
#define HEIGHT 240

void unsignedInt(BITMAP *bitmap)
{
    //Set the pixel
    int x=0, y=0;
    
    unsigned int *pixels = (unsigned int*)bitmap->dat;
    unsigned int r=0, g=0, b=0, N=0, color=0;

    for(y=0; y<HEIGHT; y++)
    {
        for(x=0; x<WIDTH; x++)
        {
            /**
             * Because ints are stored from right to left, we need to 
             * bitshift each value the according amount of bits backwards.
             **/
            #ifdef ALLEGRO_BIG_ENDIAN
                color = ((b & 0xff) >> 16)+((MIN(y, 255) & 0xff) >> 8)+((MIN(x, 255) & 0xff));   //BGR
            #else
                color = ((MIN(x, 255) & 0xff) << 16) + ((MIN(y, 255) & 0xff) << 8) + (b & 0xff);    //RGB
            #endif

            N = ( y * WIDTH)  + x;
            
            /**
             * Example: Yellow is 255,239,0 is "0xffef00"
             *
             * R (255 & 0xff) << 16 = ff0000
             * G (239 & 0xff) << 8 = ef00
             * B (0 & 0xff) = 0
             **/

            //~ pixels[N] = 0xFF0000;
            pixels[N] = color;
        }
    }
}

void unsignedChar(BITMAP *bitmap)
{
    //Set the pixel
    int x=0, y=0;
    //~ unsigned int bpp = bitmap_color_depth(bitmap);
    unsigned int bpp = 4;
    
    unsigned char* pixels = (unsigned char*)bitmap->dat;
    unsigned char r=0, g=0, b=0, color=0;
    unsigned int N=0;

    for(y=0; y<HEIGHT; y++)
    {
        for(x=0; x<WIDTH; x++)
        {
            r = ((MIN(x, 255) & 0xff));
            g = ((MIN(y, 255) & 0xff));
            b = (b & 0xff);

            /**
             * RGB images are usually stored in interleaved order 
             * (R1, G1, B1, R2, G2, B2, ...)
             * 
             * Pixel with index N is stored at:
             *  pixels[bpp*N+1],
             *  pixels[bpp*N+2] 
             *  pixels[bpp*N+3] 
             * 
             * instead of just red[N], green[N], blue[N].
             * 
             * Example: Red is 255,0,0 is "0xff0000" 
             * 
             * A       R        G       B
             * 00      ff       00      00
             * bpp     bpp+1    bpp+2   bpp+3
             * 
             **/
            N = ( y * bitmap->w  + x );
            
            pixels[bpp * N + 0] = b;  //0 = BLUE
            pixels[bpp * N + 1] = g;  //1 = GREEN
            pixels[bpp * N + 2] = r;  //2 = RED
            pixels[bpp * N + 3] = 1;  //3 = ALPHA

            /**
             * Depending on the endianess, byte orders can be stored 
             * inverted in memory, and by fwrite too.
             *  
             * On 32bit little-endian systems we should have:
             * 00 00 ff 00
             **/

            //~ fwrite(&pixels[N * bpp], sizeof(unsigned char), 1, fp);
            //~ fwrite(&pixels[N * bpp+1], sizeof(unsigned char), 1, fp);
            //~ fwrite(&pixels[N * bpp+2], sizeof(unsigned char), 1, fp);
            //~ fwrite(&pixels[N * bpp+3], sizeof(unsigned char), 1, fp);
        }
    }
}

int main(void)
{
    if (allegro_init() != 0)
      return 1;

    install_keyboard(); 

    int color_depth = desktop_color_depth();

    set_color_depth(color_depth);
    set_gfx_mode(GFX_AUTODETECT_WINDOWED, WIDTH, HEIGHT, 0, 0);

    BITMAP *bitmap = create_bitmap_ex(color_depth, WIDTH, HEIGHT);

    //~ unsignedInt(bitmap);
    unsignedChar(bitmap);

    blit(bitmap, screen, 0, 0, 0, 0, bitmap->w, bitmap->h);

    destroy_bitmap(bitmap);

   /* wait for a key press */
   readkey();

   return 0;
}

END_OF_MAIN()
